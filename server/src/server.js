import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import morgan from 'morgan'

const app = express()
app.set('port', process.env.PORT || 3000)

app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())


app.get('/',(req , res)=>{
    res.send({
        mensaje : 'Hola Armando Michel'
    })
})

app.listen(app.get('port'), ( ) => {
    console.log(`Servidor corriendo en el puerto ${app.get('port')}`)
})